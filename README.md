# mailtrain.yml

[welder](https://gitlab.com/welder-cm/welder) script which installs mailtrain and all dependencies

## !! WARNING !!

**THIS IS >>NO<< "FIRE and FORGET" SCRIPT!** You should **know** what you are doing, how welder works and how your local environment is set up. The script just helps you with setting mailtrain up and getting a feeling of all necessary steps.

The script was originally written for the [ECG](https://www.ecogood.org) and has been adapted to some special cases of [Hostsharing](https://www.hostsharing.net). Therefore get through the file `modules/mailtrain/setup.sh` **CAREFULLY >>BEFORE<<** you start using it. Some of the paths and variables need to be adapted to your local environment. The script also **DOES NOT** check dependencies like `screen` and other tools during runtime. So please make sure all commands and tools used within the script are present otherwise the script will fail.

## !! WARNING !!

[Related blog article on mailtrain v2 installation (German only)](https://andersgood.de/blog/mailtrain-v2-installieren-und-konfigurieren)

----
