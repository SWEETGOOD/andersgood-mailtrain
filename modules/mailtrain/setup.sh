#                                                                                #
# ███████╗██╗    ██╗███████╗███████╗████████╗ ██████╗  ██████╗  ██████╗ ██████╗  #
# ██╔════╝██║    ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝ ██╔═══██╗██╔═══██╗██╔══██╗ #
# ███████╗██║ █╗ ██║█████╗  █████╗     ██║   ██║  ███╗██║   ██║██║   ██║██║  ██║ #
# ╚════██║██║███╗██║██╔══╝  ██╔══╝     ██║   ██║   ██║██║   ██║██║   ██║██║  ██║ #
# ███████║╚███╔███╔╝███████╗███████╗   ██║   ╚██████╔╝╚██████╔╝╚██████╔╝██████╔╝ #
# ╚══════╝ ╚══╝╚══╝ ╚══════╝╚══════╝   ╚═╝    ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝  #
#                                                                                #
#                                      IT-Beratung mit Fokus auf Datensicherheit #
#                                                                                #
#                            www.sweetgood.de                                    #
#                                                                                #
# Copyright        : not specified yet
# Repository url   : https://codeberg.org/SWEETGOOD/andersgood-mailtrain
# Author           : codiflow @ SWEETGOOD
# Filename         : setup.sh
# Created at       : 08.04.2022
# Last changed at  : 25.09.2022
# Description      : welder playbook for installing mailtrain

#!/usr/bin/env bash
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

# Setup colors
RED='\e[31m'
BLUE='\e[36m'
GREEN='\e[0;32m'
NC='\e[39m' # No Color

# Security query if not deploying to the testing server
if [[ ! ${W_SSH_URL} =~ .*"TESTINGSYSTEM.GREENWEBSPACE.COM".*  ]]; then
    echo " "
    echo " "
    echo -e "${RED}WARNING – it seems like you are not deploying to the testing server (ecg04).${NC}"
    echo -e "${RED}Please CAREFULLY verify the target BEFORE you continue.${NC}"
    echo " "
    read -n 1 -s -r -p "Press ANY key to start the deployment to ${W_SSH_URL}..."
fi

# -------------------------------- #

echo " "
echo -e "${BLUE}BACKING UP DATABASE (just for safety)${NC}"
echo " "

mysqldump --user=${W_MAILTRAIN_MYSQLUSER} --password=${W_MAILTRAIN_MYSQLPASSWORD} --lock-tables --databases ${W_MAILTRAIN_MYSQLDATABASE} | gzip -9 > $HOME/$(date +%Y%m%d-%H%M%S)_dump_${W_MAILTRAIN_MYSQLDATABASE}.sql.gz

# -------------------------------- #

# NODEJS

echo " "
echo -e "${BLUE}STARTING INSTALLATION OF NODE.JS${NC}"
echo " "

# Cleanup of old nvm installations
rm -rf $HOME/.nvm
rm -rf $HOME/.npm

# Install Node.js through nvm
wget https://github.com/nvm-sh/nvm/archive/refs/tags/v${W_NODE_NVM}.zip -O $HOME/nvm_v${W_NODE_NVM}.zip
unzip $HOME/nvm_v${W_NODE_NVM}.zip
rm $HOME/nvm_v${W_NODE_NVM}.zip
cd $HOME/nvm-${W_NODE_NVM}/
./install.sh
cd $HOME
rm -rf $HOME/nvm-${W_NODE_NVM}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
nvm install 14
nvm alias default 14

# -------------------------------- #

# MAILTRAIN

echo " "
echo -e "${BLUE}STARTING INSTALLATION OF MAILTRAIN${NC}"
echo " "

# Cleanup of old mailtrain installation
rm -rf $HOME/mailtrain

# If the database should be cleared beforehand
if [ ${W_MAILTRAIN_STARTWITHCLEANDATABASE} ]; then
    echo -e "${RED}Starting with a CLEAN database${NC}"
    mysqldump --add-drop-table --no-data -u ${W_MAILTRAIN_MYSQLUSER} -p${W_MAILTRAIN_MYSQLPASSWORD} ${W_MAILTRAIN_MYSQLDATABASE} | grep 'DROP TABLE' >> $HOME/clear-mailtrain-database.sql
    mysql -u ${W_MAILTRAIN_MYSQLUSER} -p${W_MAILTRAIN_MYSQLPASSWORD} ${W_MAILTRAIN_MYSQLDATABASE} < $HOME/clear-mailtrain-database.sql
    rm -rf $HOME/clear-mailtrain-database.sql
fi

# Install mailtrain

# Clone Git repository and checkout v2
mkdir $HOME/mailtrain
curl -Ss --location "https://github.com/Mailtrain-org/mailtrain/archive/refs/heads/v2.tar.gz" | tar -xzvC "$HOME/mailtrain" --strip 1

# !!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!! #
# THE FOLLOWING SECTION IS UNTESTED – MAYBE YOU MIGHT WANT TO ADAPT IT TO YOUR NEEDS!
# !!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!! #

#cat > /etc/mysql/mariadb.conf.d/60-fix-key-length.cnf <<EOT
#[mysqld]
#innodb_file_format = Barracuda
#innodb_file_per_table = on
#innodb_default_row_format = dynamic
#innodb_large_prefix = 1
#innodb_file_format_max = Barracuda
#EOT

# Change collation of database
mysql --user="${W_MAILTRAIN_MYSQLUSER}" --password="${W_MAILTRAIN_MYSQLPASSWORD}" <<EOF
ALTER DATABASE ${W_MAILTRAIN_MYSQLDATABASE} DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci;
EOF

## FUNCTIONS (need to be declared first) ##

function doForAllModules {
    # Install required node packages
    for idx in $HOME/mailtrain/client $HOME/mailtrain/shared $HOME/mailtrain/server $HOME/mailtrain/zone-mta $HOME/mailtrain/mvis/client $HOME/mailtrain/mvis/server $HOME/mailtrain/mvis/test-embed $HOME/mailtrain/mvis/ivis-core/client $HOME/mailtrain/mvis/ivis-core/server $HOME/mailtrain/mvis/ivis-core/shared $HOME/mailtrain/mvis/ivis-core/embedding; do
        if [ -d $idx ]; then
            ($1 $idx)
        fi
    done
}

function reinstallModules {
    local idx=$1
    echo Reinstalling modules in $idx
    cd $idx && rm -rf node_modules && npm install
}

function installMailtrain {
    # Setup installation configuration
    cat > $HOME/mailtrain/server/config/production.yaml <<EOT
user: ${W_MAILTRAIN_LINUXUSER}
group: ${W_MAILTRAIN_LINUXGROUP}
roUser: nobody
roGroup: nobody

www:
  host: ${W_MAILTRAIN_WWWHOST}
  proxy: ${W_MAILTRAIN_WWWPROXY}
  secret: "`pwgen -1`"
  trustedUrlBase: https://${W_DOMAIN_TRUSTED}
  sandboxUrlBase: https://${W_DOMAIN_SANDBOX}
  publicUrlBase: https://${W_DOMAIN_PUBLIC}

defaultLanguage: ${W_MAILTRAIN_DEFAULTLANGUAGE}

mysql:
  host: localhost
  user: "${W_MAILTRAIN_MYSQLUSER}"
  password: "${W_MAILTRAIN_MYSQLPASSWORD}"
  database: "${W_MAILTRAIN_MYSQLDATABASE}"
  port: 3306
  timezone: local

ldap:
  enabled: true
  method: ldapauth
  host: ${W_MAILTRAIN_LDAPHOST}
  port: ${W_MAILTRAIN_LDAPPORT}
  secure: false
  baseDN: ou=users,ou=ecg
  filter: (|(uid={{username}})(mail={{username}}))
  # Username field in LDAP (uid/cn/username)
  uidTag: uid
  # nameTag identifies the attribute to be used for user's full name
  nameTag: cn
  # mailTag identifies the attribute to be used for user's email address
  mailTag: mail
  passwordresetlink: "${W_MAILTRAIN_PASSWORDRESETLINK}"
  newUserRole: ${W_MAILTRAIN_NEWUSERROLE}
  # Global namespace id
  newUserNamespaceId: 1
  # Use a different user to bind LDAP (final bind DN will be: {{uidTag}}: {{bindUser}},{{baseDN}})
  bindUser: ${W_MAILTRAIN_LDAPBINDUSER}
  bindPassword: ${W_MAILTRAIN_LDAPBINDPASSWORD}

redis:
  enabled: true
  port: ${W_REDIS_PORT}

log:
  level: info

reports:
  enabled: ${W_MAILTRAIN_REPORTS}

builtinZoneMTA:
  enabled: ${W_MAILTRAIN_USEBUILTINZONEMTA}
  mongo: mongodb://127.0.0.1:${W_MONGO_PORT}/zone-mta
  redis: redis://localhost:${W_REDIS_PORT}/2
  log:
    level: warn

queue:
  processes: 5
EOT

    # !!! WARNING !!!
    # In the original documentation they use a read-only MySQL user here for security reasons
    # But as Hostsharing doesn't allow two users with different permissions to be able to administer a database
    # we used the same credentials as for the service.
    # This should be changed as soon as possible.
    # !!! WARNING !!!

    # This is only necessary if using the reports feature
    if [ ${W_MAILTRAIN_REPORTS} ]; then
        cat >> $HOME/mailtrain/server/services/workers/reports/config/production.yaml <<EOT
log:
  level: warn

mysql:
  user: "${W_MAILTRAIN_MYSQLROUSER}"
  password: "${W_MAILTRAIN_MYSQLROPASSWORD}"
EOT
    fi

    doForAllModules reinstallModules

    (cd $HOME/mailtrain/client && npm run build)

    chown -R ${W_MAILTRAIN_LINUXUSER}:${W_MAILTRAIN_LINUXGROUP} $HOME/mailtrain
    chmod o-rwx $HOME/mailtrain/server/config
}

installMailtrain

# Install missing passport-ldapauth
cd $HOME/mailtrain/server ; npm install passport-ldapauth

# -------------------------------- #

if [ ${W_MAILTRAIN_USEBUILTINZONEMTA} = "true"  ]; then
    echo " "
    echo -e "${BLUE}STARTING INSTALLATION OF MONGODB${NC}"
    echo " "

    # Cleanup of old mongodb installation
    rm -rf $HOME/mongodb
    rm -rf $HOME/mongodb-data
    rm -rf $HOME/mongodb-logs

    # Kill old MongoDB daemon if running
    if [ `pidof $HOME/mongodb/bin/mongod` ]; then
        kill `pidof $HOME/mongodb/bin/mongod`
    fi

    # Install MongoDB
    wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-debian10-${W_MONGO_VERSION}.tgz -O $HOME/mongodb-linux-x86_64-debian10-${W_MONGO_VERSION}.tgz
    tar -xzvf $HOME/mongodb-linux-x86_64-debian10-${W_MONGO_VERSION}.tgz
    rm $HOME/mongodb-linux-x86_64-debian10-${W_MONGO_VERSION}.tgz
    mv $HOME/mongodb-linux-x86_64-debian10-${W_MONGO_VERSION} $HOME/mongodb
    export PATH=$HOME/mongodb/bin:$PATH
    mkdir -p $HOME/mongodb-data/db
    mkdir $HOME/mongodb-logs

    echo " "
    echo -e "${BLUE}STARTING MONGODB${NC}"
    echo " "

    # Kill old mongodb instance if necessary
    if [ `pidof mongod` ]; then
        kill `pidof mongod`
    fi

    # Start MongoDB in background
    $HOME/mongodb/bin/mongod --dbpath $HOME/mongodb-data/db -logpath $HOME/mongodb-logs/mongodb.log --port ${W_MONGO_PORT} --fork
fi

# -------------------------------- #

echo " "
echo -e "${BLUE}STARTING REDIS-SERVER${NC}"
echo " "

# Kill old redis instance if necessary
if [ `pidof redis-server` ]; then
    kill `pidof redis-server`
    # Not nice but working...
    killall -9 redis-server
fi

# Start Redis server in the background
redis-server --port ${W_REDIS_PORT} --daemonize yes

# -------------------------------- #

echo " "
echo -e "${BLUE}DEPLOYING HTACCESS FILES${NC}"
echo " "

# Deploy .htaccess files
rm -rf /var/www/html/${W_DOMAIN_TRUSTED}/htdocs-ssl/.htaccess
cat >> /var/www/html/${W_DOMAIN_TRUSTED}/htdocs-ssl/.htaccess <<EOT
RewriteEngine on
RewriteBase /
RewriteRule (.*) http://127.0.0.1:3000/\$1 [P]
EOT

rm -rf /var/www/html/${W_DOMAIN_SANDBOX}/htdocs-ssl/.htaccess
cat >> /var/www/html/${W_DOMAIN_SANDBOX}/htdocs-ssl/.htaccess <<EOT
RewriteEngine on
RewriteBase /
RewriteRule (.*) http://127.0.0.1:3003/\$1 [P]
EOT

rm -rf /var/www/html/${W_DOMAIN_PUBLIC}/htdocs-ssl/.htaccess
cat >> /var/www/html/${W_DOMAIN_PUBLIC}/htdocs-ssl/.htaccess <<EOT
RewriteEngine on
RewriteBase /
RewriteRule (.*) http://127.0.0.1:3004/\$1 [P]
EOT

# -------------------------------- #

echo " "
echo -e "${BLUE}STARTING MAILTRAIN${NC}"
echo " "

# Kill old instance of screen
if [ `pidof SCREEN` ]; then
    kill `pidof SCREEN`
fi

if [ `pidof screen` ]; then
    kill `pidof screen`
fi

# Start mailtrain in the background inside of screen
screen -dm bash -c 'cd $HOME/mailtrain/server ; export NODE_ENV="production" ; $NVM_BIN/node index.js ; exec sh'

# Display IMPORTANT information
echo -e "${RED}!!! IMPORTANT INFORMATION !!!${NC}"

cat <<EOT
Run service with activated LDAP auth and authenticate with your LDAP admin username
Then shut down the service and deactivate LDAP auth temporarily (switch to localauth)
Run service without LDAP authentication and authenticate using 'admin' and 'test'
!!! CHANGE !!! the local admin password
Gain your LDAP admin username admin privileges by setting the role to "Global master" and logout afterwards
Run service with activated LDAP auth and authenticate with your LDAP admin username
You should now have admin privileges"
EOT

echo " "
echo -e "${GREEN}INSTALLATION FINISHED${NC}"
